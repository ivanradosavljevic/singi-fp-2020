/*
 * Napisati program koji u odvojenom worker thread-u
 * računa kvadrate elemenata niza i potom ispisuje rezultat.
 */

const { Worker } = require("worker_threads");

const worker = new Worker("./kvadrat.js", { workerData: { niz: Array(10000000).fill(1).map(x => Math.random() * 10) } });
console.log("Kreiran worker");
console.log("Racunanje u toku....");

worker.on("message", r => {
    console.log(r);
});

worker.on("exit", code => {
    console.log(code);
});